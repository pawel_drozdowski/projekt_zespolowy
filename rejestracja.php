<?php

	session_start();
	if(isset($_POST['email']))
	{
		//udana walizacja ? 
		$wszystko_OK=true;
		//sprawdzanie nickname
		$nick=$_POST['nick'];
		//sprawdzenie długości nicku
		if((strlen($nick)<3)|| (strlen($nick)>20))
		{
			$wszystko_OK=false;
			$_SESSION['e_nick']="Nickname musi posiadać od 3 do 20 znaków!";
		}
		
		if(ctype_alnum($nick)==false)
		{
			$wszystko_OK=false;
			$_SESSION['e_nick']="nick może składać się tylko z liter i cyfr(bez znaków polskich)";
		}
		
		//Sprawdż poprawność adresu email
		$email=$_POST['email'];
		$emailB=filter_var($email,FILTER_SANITIZE_EMAIL);
		if((filter_var($emailB,FILTER_VALIDATE_EMAIL)==false)||($emailB!=$email))
		{
			$wszystko_OK=false;
			$_SESSION['e_email']="Podaj poprawny adres e-mail!";
		}
		
		//sprawdz poprawnosc hasła
		$haslo1=$_POST['haslo1'];
		$haslo2=$_POST['haslo2'];
		
		if((strlen($haslo1)<8)||(strlen($haslo1)>20))
		{
			$wszystko_OK=false;
			$_SESSION['e_haslo']="Hasło musi posiadać od 8 do 20 znaków!";
		}
		
		if($haslo1!=$haslo2)
		{
			$wszystko_OK=false;
			$_SESSION['e_haslo']="Podane hasła nie są takie same!";
		}
		
		$haslo_hash= password_hash($haslo1,PASSWORD_DEFAULT);

		//akceptacja regulaminu?
		if(!isset($_POST['regulamin']))
		{
			$wszystko_OK=false;
			$_SESSION['e_regulamin']="Regulamin nie został zaakceptowany!";
		}
		
		//Recaptcha on/of
		$sekret="6LeTG1AfAAAAAJJBpVVg8JUtMecx4sMmcQKAKd4w";
		$sprawdz=file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$sekret.'&response='.$_POST['g-recaptcha-response']);
		
		$odpowiedz=json_decode($sprawdz);
		
		if($odpowiedz->success==false)
		{
			$wszystko_OK=false;
			$_SESSION['e_bot']="Potwierdź, że nie jesteś botem!";
		}
		
		require_once "connect.php";
		mysqli_report(MYSQLI_REPORT_STRICT);
		
		try
		{
			$polaczenie = new mysqli($host, $db_user,$db_password, $db_name);
			if($polaczenie->connect_errno!=0)
			{
				throw new Exception(msqli_connect_errno());
			}
			else
			{
				//czy email już istnieje?
				$rezultat=$polaczenie->query("SELECT id FROM uzytkownicy WHERE email='$email'");
				
				if(!$rezultat) throw new Exception($polaczenie->error);

				$ile_takich_maili = $rezultat->num_rows;
                if($ile_takich_maili>0)
                {
                    $wszystko_OK=false;
                    $_SESSION['e_email']="Istnieje już konto przypisane do tego adresu e-mail!";
                }
				
				//czy login już istnieje?
				$rezultat=$polaczenie->query("SELECT id FROM uzytkownicy WHERE user='$nick'");
			
				if(!$rezultat) throw new Exception($polaczenie->error);

				$ile_takich_nickow = $rezultat->num_rows;
                if($ile_takich_nickow>0)
                {
                    $wszystko_OK=false;
                    $_SESSION['e_nick']="Login jest już zajęty!";
                }
				
				if($wszystko_OK==true)
				{
					//zaliczone,dodajemy użytkownika do bazy
					
					if($polaczenie->query("INSERT INTO uzytkownicy VALUES(NULL,'$nick','$haslo_hash','$email','uzytkownik')"))
					{
						$_SESSION['udanarejestracja']=true;
						header('Location:zakonczenie.php');
					}
					else
					{
						throw new Exception($polaczenie->error);
					}
					
					
				}
					
				
				$polaczenie->close();
			}
		}
		catch(Exception $e)
		{
			echo '<span style="color:red;">Błąd serwera! Przepraszamy serwer nie działa prosimy spróbować poźniej</span>';
			//echo '<br/>Informacja developerska:'.$e;
		}
		
	}
	
?>

<!DOCTYPE HTML>
<html lang="pl">
<head>
<link rel="stylesheet" href="style.css" type="text/css"/>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<title> Strona Streamingowa-załóż darmowe konto!</title>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>

	<style>
		.error
		{
			color:red;
			margin_top: 10px;
			margin_bottom: 10px;
		}
	
	</style>

</head>
<body>

<div id="container">

<div id="logo">

	<h1>Rejestracja - Projekt</h1>

</div>

<div id="nav">
<img src="reklama.jpg"/>
</div>

<div id="content">
<form method="post">
	
	Login:<br/><input type="text" name="nick"/><br/>
	
	<?php
	if(isset($_SESSION['e_nick']))
	{
		echo '<div class="error">'.$_SESSION['e_nick'].'</div>';
		unset($_SESSION['e_nick']);
	}
	?>
	
	E-mail:<br/><input type="text" name="email"/><br/>
	
	<?php
	if(isset($_SESSION['e_email']))
	{
		echo '<div class="error">'.$_SESSION['e_email'].'</div>';
		unset($_SESSION['e_email']);
	}
	?>
	
	Hasło:<br/><input type="password" name="haslo1"/><br/>
	
	<?php
	if(isset($_SESSION['e_haslo']))
	{
		echo '<div class="error">'.$_SESSION['e_haslo'].'</div>';
		unset($_SESSION['e_haslo']);
	}
	?>
	
	Powtórz hasło:<br/><input type="password" name="haslo2"/><br/>
	
	<label>
	<input type="checkbox" name="regulamin" />Akceptuje regulamin
	</label>
	
	<?php
	if(isset($_SESSION['e_regulamin']))
	{
		echo '<div class="error">'.$_SESSION['e_regulamin'].'</div>';
		unset($_SESSION['e_regulamin']);
	}
	?>
	
	<div class="g-recaptcha" data-sitekey="6LeTG1AfAAAAAEXut-PSJ6RgbjlQBVnfbEv-PB_J"></div>
      <br/><br/>
	  <?php
	if(isset($_SESSION['e_bot']))
	{
		echo '<div class="error">'.$_SESSION['e_bot'].'</div>';
		unset($_SESSION['e_bot']);
	}
	?>
      <input type="submit" value="Zarejestruj się">
	  
	</form>

</div>

<div id="ad">
<img src="reklama.jpg"/>
</div>

<div id="footer">
		Najlepszy Projekt &copy;
		</div>

</div>

	
</body>
</html>